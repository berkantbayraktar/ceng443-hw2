import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;

public class Simulator {
    public static void main(String args[]) {
        int smelterNumber, constructorNumber, transporterNumber;
        List<Runnable> smelters = new ArrayList<>();
        List<Runnable> transporters = new ArrayList<>();
        List<Runnable> constructors = new ArrayList<>();
        List<ProductionBuffer> productionBuffers = new ArrayList<>();
        List<AssemblyBuffer> assemblyBuffers = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);

        smelterNumber = scanner.nextInt();
        for(int i = 0 ; i < smelterNumber ; i++){
            int sleepTime = scanner.nextInt(); int storageCapacity = scanner.nextInt(); int ingotType = scanner.nextInt(); int productionLimit = scanner.nextInt();

            productionBuffers.add( new ProductionBuffer(i+1, new Semaphore(storageCapacity), new Semaphore(0), new Semaphore(0)));
            smelters.add(new Smelter(i+1,sleepTime, storageCapacity, ingotType, productionLimit, productionBuffers.get(i)));
        }

        constructorNumber = scanner.nextInt();
        for(int i = 0 ; i < constructorNumber ; i++){
            int sleepTime = scanner.nextInt(); int storageCapacity = scanner.nextInt(); int ingotType = scanner.nextInt();
            assemblyBuffers.add(new AssemblyBuffer(i+1, new Semaphore(0), new Semaphore(storageCapacity), new Semaphore(0)));
            constructors.add(new Constructor(i+1, sleepTime, storageCapacity, ingotType, assemblyBuffers.get(i)));
        }

        transporterNumber = scanner.nextInt();
        for(int i = 0; i < transporterNumber ; i++){
            int sleepTime = scanner.nextInt(); int smelterID = scanner.nextInt(); int constructorID = scanner.nextInt();
            transporters.add(new Transporter(i+1, sleepTime, smelterID , constructorID, productionBuffers.get(smelterID-1), assemblyBuffers.get(constructorID-1)));
        }

        ExecutorService taskList = Executors.newFixedThreadPool(smelterNumber + transporterNumber + constructorNumber);
        HW2Logger.InitWriteOutput();
        smelters.forEach((smelter)-> taskList.execute(smelter));
        constructors.forEach((constructor)-> taskList.execute(constructor));
        transporters.forEach((transporter)->taskList.execute(transporter));
        taskList.shutdown();
    }

}
