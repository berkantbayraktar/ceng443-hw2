/**
 * @author Berkant Bayraktar
 */

public class Transporter implements Runnable {
    private final int id;
    private final int sleepTime;
    private final int smallerID;
    private final int constructorID;
    private ProductionBuffer productionBuffer;
    private AssemblyBuffer assemblyBuffer;


    public Transporter(int id, int sleepTime, int smallerID, int constructorID, ProductionBuffer productionBuffer, AssemblyBuffer assemblyBuffer) {
        this.id = id;
        this.sleepTime = sleepTime;
        this.smallerID = smallerID;
        this.constructorID = constructorID;
        this.productionBuffer = productionBuffer;
        this.assemblyBuffer = assemblyBuffer;
    }

    @Override
    public void run(){
        try{
            productionBuffer.startTransporter(id);
            while( (productionBuffer.getSmelterStopped().availablePermits() == 0  || productionBuffer.getIngotProduced().availablePermits() > 0 )
            && assemblyBuffer.getConstructorStopped().availablePermits() == 0){ // FIX HERE (fixed maybe)
                boolean isAcquired = productionBuffer.travelToSmelter(id, sleepTime);
                if(!isAcquired)
                    continue;
                productionBuffer.takeIngot(id, sleepTime);
                assemblyBuffer.travelToConstructor(id, sleepTime);
                assemblyBuffer.dropIngot(id, sleepTime);
            }
            assemblyBuffer.stopTransporter(id); //FIX HERE ( fixed maybe)
        }
        catch (Exception e){}
    }

    @Override
    public String toString() {
        return "Transporter{" +
                "id=" + id +
                ", sleepTime=" + sleepTime +
                ", smallerID=" + smallerID +
                ", constructorID=" + constructorID +
                ", productionBuffer=" + productionBuffer +
                ", assemblyBuffer=" + assemblyBuffer +
                '}';
    }
}
