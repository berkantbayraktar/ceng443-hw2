import java.sql.Time;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.DoubleStream;

public class AssemblyBuffer {
    private final int constructorId;
    private Semaphore waitIngots;
    private Semaphore constructorProduced;
    private Semaphore constructorStopped;

    public AssemblyBuffer(int constructorId, Semaphore waitIngots, Semaphore constructorProduced, Semaphore constructorStopped) {
        this.constructorId = constructorId;
        this.waitIngots = waitIngots;
        this.constructorProduced = constructorProduced;
        this.constructorStopped = constructorStopped;
    }




    public void assembleIron(int sleepTime) throws  Exception{
        boolean notTimeout = waitIngots.tryAcquire(2,3,TimeUnit.SECONDS);
        if(!notTimeout)
            throw new TimeoutException();

        HW2Logger.WriteOutput(0, 0, constructorId, Action.CONSTRUCTOR_STARTED);
        sleep(sleepTime);
        constructorProduced.release(2);
        HW2Logger.WriteOutput(0, 0, constructorId, Action.CONSTRUCTOR_FINISHED);
    }

    public void assembleCopper(int sleepTime) throws  Exception{
        boolean notTimeout = waitIngots.tryAcquire(3,3,TimeUnit.SECONDS);
        if(!notTimeout)
            throw new TimeoutException();
        HW2Logger.WriteOutput(0, 0, constructorId, Action.CONSTRUCTOR_STARTED);
        sleep(sleepTime);
        constructorProduced.release(3);
        HW2Logger.WriteOutput(0, 0, constructorId, Action.CONSTRUCTOR_FINISHED);
    }

    public void travelToConstructor(int transporterId, int sleepTime)throws  Exception{
       constructorProduced.acquire();
        HW2Logger.WriteOutput(0, transporterId, constructorId, Action.TRANSPORTER_TRAVEL);
        sleep(sleepTime); // FIX HERE
    }

    public void dropIngot(int transporterId, int sleepTime) throws  Exception{
        HW2Logger.WriteOutput(0, transporterId, constructorId, Action.TRANSPORTER_DROP_INGOT);
        sleep(sleepTime); // FIX HERE
        waitIngots.release();
    }

    public void stopTransporter(int transporterId){
        HW2Logger.WriteOutput(0, transporterId, 0, Action.TRANSPORTER_STOPPED);
    }

    public void startConstructor(){
        HW2Logger.WriteOutput(0,0, constructorId, Action.CONSTRUCTOR_CREATED);
    }

    public void stopConstructor(){
        constructorStopped.release();
        HW2Logger.WriteOutput(0, 0, constructorId, Action.CONSTRUCTOR_STOPPED);
    }

    public Semaphore getConstructorStopped() {
        return constructorStopped;
    }

    public void sleep(int sleepTime) throws Exception{

        Random random = new Random(System.currentTimeMillis());
        DoubleStream stream;
        stream = random.doubles(1, sleepTime-sleepTime*0.01, sleepTime+sleepTime*0.02);
        Thread.sleep((long) stream.findFirst().getAsDouble());
    }

    @Override
    public String toString() {
        return "AssemblyBuffer{" +
                "constructorId=" + constructorId +
                ", waitIngots=" + waitIngots +
                ", constructorProduced=" + constructorProduced +
                ", constructorStopped=" + constructorStopped +
                '}';
    }
}



