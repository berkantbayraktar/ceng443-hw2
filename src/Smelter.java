/**
 * @author Berkant Bayraktar
 */

public class Smelter implements Runnable {
    private final int id;
    private final int sleepTime;
    private final int storageCapacity;
    private final int ingotType;
    private final int productionLimit;
    private ProductionBuffer productionBuffer;



    public Smelter(int id, int sleepTime, int storageCapacity, int ingotType, int productionLimit, ProductionBuffer productionBuffer) {
        this.id = id;
        this.sleepTime = sleepTime;
        this.storageCapacity = storageCapacity;
        this.ingotType = ingotType;
        this.productionLimit = productionLimit;
        this.productionBuffer = productionBuffer;
    }

    @Override
    public void run() {
        try{
            productionBuffer.startSmelter();
            for(int i = 0 ; i < productionLimit ; i++){
                productionBuffer.produce(sleepTime);
            }
            productionBuffer.stopSmelter();
       }
       catch (Exception e){}
    }

    @Override
    public String toString() {
        return "Smelter{" +
                "id=" + id +
                ", sleepTime=" + sleepTime +
                ", storageCapacity=" + storageCapacity +
                ", ingotType=" + ingotType +
                ", productionLimit=" + productionLimit +
                ", ingotProducet= " + productionBuffer.getIngotProduced() +
                '}';
    }
}
