/**
 * @author Berkant Bayraktar
 */

public class Constructor implements  Runnable {
    private final int id;
    private final int sleepTime;
    private final int storageCapacity;
    private final int ingotType;
    private AssemblyBuffer assemblyBuffer;

    public Constructor(int id, int sleepTime, int storageCapacity, int ingoType, AssemblyBuffer assemblyBuffer) {
        this.id = id;
        this.sleepTime = sleepTime;
        this.storageCapacity = storageCapacity;
        this.ingotType = ingoType;
        this.assemblyBuffer = assemblyBuffer;
    }

    @Override
    public void run() {
        try{
            assemblyBuffer.startConstructor();
            while (true){ // FIX HERE
                if(ingotType == 0)
                    assemblyBuffer.assembleIron(sleepTime);
                else if (ingotType == 1)
                    assemblyBuffer.assembleCopper(sleepTime);

            }
        }
        catch (Exception e){
            assemblyBuffer.stopConstructor();
        }
    }

    @Override
    public String toString() {
        return "Constructor{" +
                "id=" + id +
                ", sleepTime=" + sleepTime +
                ", storageCapacity=" + storageCapacity +
                ", ingoType=" + ingotType +
                '}';
    }
}
