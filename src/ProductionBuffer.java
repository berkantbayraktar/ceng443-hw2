import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.stream.DoubleStream;

public class ProductionBuffer {
    private final int smelterId;
    private Semaphore waitCanProduce;
    private Semaphore ingotProduced;
    private Semaphore smelterStopped;

    public ProductionBuffer(int smelterId, Semaphore waitCanProduce, Semaphore ingotProduced, Semaphore smelterStopped) {
        this.smelterId = smelterId;
        this.waitCanProduce = waitCanProduce;
        this.ingotProduced = ingotProduced;
        this.smelterStopped = smelterStopped;
    }

    public void produce(int sleepTime) throws Exception{
        waitCanProduce.acquire();
        HW2Logger.WriteOutput(smelterId,0,0,Action.SMELTER_STARTED);
        sleep(sleepTime); // FIX HERE
        ingotProduced.release();
        HW2Logger.WriteOutput(smelterId,0,0,Action.SMELTER_FINISHED);
        sleep(sleepTime); // FIX HERE
    }

    public boolean travelToSmelter(int transporterId, int sleepTime) throws Exception{
        boolean isAcquired = ingotProduced.tryAcquire();
        if(!isAcquired)
            return  false;

        HW2Logger.WriteOutput(smelterId, transporterId, 0, Action.TRANSPORTER_TRAVEL);
        sleep(sleepTime); // FIX HERE
        return true;
    }

    public void takeIngot(int transporterId, int sleepTime) throws  Exception{
        HW2Logger.WriteOutput(smelterId, transporterId, 0, Action.TRANSPORTER_TAKE_INGOT);
        sleep(sleepTime); // FIX HERE
        waitCanProduce.release();
    }

    public void startTransporter(int transporterId){
        HW2Logger.WriteOutput(0, transporterId, 0, Action.TRANSPORTER_CREATED);
    }

    public void startSmelter(){
        HW2Logger.WriteOutput(smelterId,0,0,Action.SMELTER_CREATED);
    }

    public void stopSmelter(){
        smelterStopped.release();
        HW2Logger.WriteOutput(smelterId,0,0,Action.SMELTER_STOPPED);
    }

    public Semaphore getSmelterStopped() {
        return smelterStopped;
    }

    public Semaphore getIngotProduced() { return ingotProduced; }

    public void sleep(int sleepTime) throws Exception{

        Random random = new Random(System.currentTimeMillis());
        DoubleStream stream;
        stream = random.doubles(1, sleepTime-sleepTime*0.01, sleepTime+sleepTime*0.02);
        Thread.sleep((long) stream.findFirst().getAsDouble());
    }

    @Override
    public String toString() {
        return "ProductionBuffer{" +
                "smelterId=" + smelterId +
                ", waitCanProduce=" + waitCanProduce +
                ", ingotProduced=" + ingotProduced +
                ", smelterStopped=" + smelterStopped +
                '}';
    }
}

